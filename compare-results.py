#!/usr/bin/env python3
import sys, os
import argparse
import re
import yaml
from pathlib import Path
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

NORMALIZE = True

_RE_COMP = re.compile(r'.*((gcc|clang)-\d+).*')
_RE_YEAR = re.compile(r'.*, (\d+)')

def parse_logs(log_files):
    benchs = None
    compilers = {}
    normalized = {}
    first = True
    for log_file in log_files:
        comp = Path(log_file).stem
        with open(log_file) as logf:
            obj = yaml.safe_load(logf)
            compilers[comp] = []
            if first:
                benchs = {k: [] for k in sorted(obj.keys()) if len(obj[k]) > 0}
            for i, bench in enumerate(benchs):
                timings = obj.get(bench, [])
                timing = 0 if len(timings) == 0 else np.average(timings)
                if first:
                    normalized.update({bench: timing})
                benchs[bench].append(timing)
                compilers[comp].append(timing)
                if NORMALIZE:
                    benchs[bench][-1] = normalized[bench] / benchs[bench][-1]
                    compilers[comp][-1] = normalized[bench] / compilers[comp][-1]
                    
        first = False
    #print(f"benchs: {benchs}")
    #print(f"compilers: {compilers}")
    #print(f"normalized: {normalized}")
    return benchs, compilers

def parse_comp_versions(compilers):
    comp_versions = [_RE_COMP.sub(r'\1', comp) for comp in compilers]
    comps = list({ver.split("-")[0]: None for ver in comp_versions})

    releases_files = [Path(__file__).parent / "installs" / f"{comp}-releases.txt" for comp in comps]
    df_releases = pd.concat([pd.read_csv(inf, sep="\t") for inf in releases_files])
    comp_names = [_RE_COMP.sub(r'\1', x) for x in df_releases['Release']]
    comp_dates = [_RE_YEAR.sub(r'\1', x) for x in df_releases['Date']]
    comp_years = dict(zip(comp_names, comp_dates))

    versions = {c: v for c, v in zip(compilers, comp_versions)}
    years = {c: comp_years.get(v, 9999) for c, v in zip(compilers, comp_versions)}
    return comps, versions, years

def parse_comp_sizes(compilers):
    comp_versions = [_RE_COMP.sub(r'\1', comp) for comp in compilers]
    comps = list({ver.split("-")[0]: None for ver in comp_versions})
    sizes_files = [Path(__file__).parent / "archives" / f"{comp}-sizes.txt" for comp in comps]
    df_sizes = pd.concat([pd.read_csv(inf, sep="\t") for inf in sizes_files])
    comp_names = [_RE_COMP.sub(r'\1', x) for x in df_sizes['Release']]
    comp_sizes = [float(x/1024) for x in df_sizes['Size']]
    all_sizes = dict(zip(comp_names, comp_sizes))
    sizes = {c: all_sizes.get(c, 0) for c in comp_versions}
    return sizes

def main(argv):
    parser = argparse.ArgumentParser(description="Benchmarks comparison")
    parser.add_argument("--bench", default="some", help="benchmarks name for the title")
    parser.add_argument("--size", action='store_true', help="output compiler size graph instead")
    parser.add_argument("-o", "--output", help="output figure (.svg or .png")
    parser.add_argument("logs", nargs='+', help="benchmarks yaml log files")
    args = parser.parse_args(argv[1:])

    benchs, compilers = parse_logs(args.logs)

    comps, versions, years = parse_comp_versions(compilers)
    comp_labels = [f"{versions[c]}\n{years[c]}" for c in compilers]
    comps_str = "/".join(comps)

    df_bench = pd.DataFrame(benchs)
    #print(df_bench.head())

    df_compilers = pd.DataFrame(compilers)
    #print(df_compilers)

    comp_sizes = parse_comp_sizes(compilers)
    df_sizes = pd.DataFrame({"Release": comp_sizes.keys(), "Size": comp_sizes.values()})
    #print(df_sizes)

    dpi = plt.rcParams['figure.dpi']

    if args.size:
        ax = sns.lineplot(data=df_sizes, marker='o', legend=False)
        plt.title(f"Evolution of the compiler size over {comps_str} versions")
        plt.xticks(range(len(comp_labels)), comp_labels)
        plt.ylim(ymin=0)
        plt.ylabel("Size of the compiler executable (MB)")
        plt.xlabel("Compilers versions")
        plt.gcf().set_size_inches(1024/dpi, 768/dpi)
        plt.tight_layout()
        if args.output:
            plt.savefig(args.output, bbox_inches='tight')
        plt.show()
    else:
        ms = plt.rcParams['lines.markersize'] ** 2
        ax = sns.scatterplot(data=df_bench, s=ms*2)
        plt.legend(loc='upper left', bbox_to_anchor=(1, 1), fontsize="8")
        ax = sns.boxplot(data=df_compilers, fliersize=0)
        plt.xticks(range(len(comp_labels)), comp_labels)
        plt.title(f"Evolution of performance of {args.bench} benchmark over {comps_str} versions")
        plt.ylabel("Speedup compared to initial compiler")
        plt.xlabel("Compilers versions")
        plt.gcf().set_size_inches(1024/dpi, 768/dpi)
        plt.tight_layout()
        if args.output:
            plt.savefig(args.output, bbox_inches='tight')
        plt.show()

    
if __name__ == "__main__":
    main(sys.argv)
