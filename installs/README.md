Install compilers
=================

First install the compiler explorer infra tools

    # Clone ce infra
    git clone --depth 100 https://github.com/compiler-explorer/infra.git

    # Set a pyenv with a python 3.9+
    pyenv virtualenv 3.10.8 compiler-explorer
    pyenc activate compiler-explorer
    mkdir -p infra/.venv # Fix issue with the infra build with pyenv

    # Or if the sytem python is 3.9+, a bare virtualenv
    python3 -m venv infra/.venv
    source infra/.venv/bin/activate

    # Download dependencies and tools
    make -C infra ce


Then install the gcc and llvm version, will be installed in `$HOME/work/compiler-explorer/install`:

    for v in 4.1.2 5.1.0 6.1.0 7.1.0 8.1.0 9.1.0 10.1.0 11.1.0 12.1.0 13.1.0; do \
        ./install-ce-gcc.sh $v; \
    done
    for v in 3.0 4.0.0 6.0.0 8.0.0 10.0.0 12.0.0 14.0.0 16.0.0; do \
        ./install-ce-clang.sh $v; \
    done

    # fixup some spurious clang installs
    mv ~/work/compiler-explorer/install/clang+llvm-3.0* ~/work/compiler-explorer/install/clang-3.0.0
    rm -rf ~/work/compiler-explorer/install/clang-assertions-4.0.0


Last, exit from the virtual environment:

    # With pyenv
    pyenv deactivate

    # Or with virtualenv
    deactivate


Note that the releases files are tabulated csv used for
matching versions to dates:

- gcc-releases.txt: from https://gcc.gnu.org/releases.html
- clang-releases.txt: from https://en.wikipedia.org/wiki/Clang