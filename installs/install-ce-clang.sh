#!/usr/bin/env bash
set -euo pipefail
dir="$(dirname "$0")"
version="${1?}"
compiler="${2-compilers/c++/clang}"
echo "Installing $compiler version $version"
"$dir"/infra/bin/ce_install --dest $HOME/work/compiler-explorer/install --staging-dir $HOME/work/compiler-explorer/staging install "$compiler" "$version"
