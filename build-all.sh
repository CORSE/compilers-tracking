#!/usr/bin/env bash
set -euo pipefail
dir="$(dirname "$0")"

sources="$dir"/polybench-c-4.2.1-beta
categories=('linear-algebra/blas'
            'linear-algebra/kernels'
            'linear-algebra/solvers'
            'datamining'
            'stencils'
            'medley')

date="$(date +%Y%m%d-%H%M%S)"
outdir="${1-outputs/$date}"

CC="${CC-gcc}"
CC_KERNEL="${CC_KERNEL-$CC}"
LD="${LD-$CC}"
CFLAGS="-O2 -std=gnu99 -DPOLYBENCH_USE_C99_PROTO -DPOLYBENCH_TIME ${CFLAGS-}"
CFLAGS_KERNEL="$CFLAGS ${CFLAGS_KERNEL-}"
LDFLAGS="-lm ${LDFLAGS-}"

rm -rf "$outdir" output
mkdir -p "$outdir"
ln -s "$outdir" output

polybench="$outdir"/polybench
cp -pr "$sources" "$polybench"
for cat in "${categories[@]}"; do
    for kerndir in "$polybench/$cat"/*; do
        kern="$(basename "$kerndir")"
        utilsdir=../../utilities
        [ -d "$kerndir/$utilsdir" ] || utilsdir=../../../utilities
        echo "Generating Makefile for $kerndir ($utilsdir)..."
        cat >"$kerndir"/Makefile <<EOF
CC=$CC
CC_KERNEL=$CC_KERNEL
LD=$LD
CFLAGS=$CFLAGS
CFLAGS_KERNEL=$CFLAGS_KERNEL
LDFLAGS=$LDFLAGS
UTILS_DIR=$utilsdir

all: $kern

$kern: $kern.o polybench.o
	\$(LD) -o $kern $kern.o polybench.o \$(CFLAGS) \$(LDFLAGS)


$kern.i: $kern.c $kern.h \$(UTILS_DIR)/polybench.h
	\$(CC) -E -o $kern.i $kern.c -I. -I\$(UTILS_DIR) \$(CFLAGS)
	sed -i 's|_Float128|double|g' $kern.i

$kern.o: $kern.i
	\$(CC_KERNEL) -c -o $kern.o $kern.i \$(CFLAGS_KERNEL)

polybench.o: \$(UTILS_DIR)/polybench.c \$(UTILS_DIR)/polybench.h
	\$(CC) -c -o polybench.o \$(UTILS_DIR)/polybench.c -I. -I\$(UTILS_DIR) \$(CFLAGS)

clean:
	rm -f *.o *.i $kern

.PHONY: all clean
.SUFFIXES:
EOF
        echo "Building $kerndir..."
        make -C "$kerndir" clean all
    done
done
