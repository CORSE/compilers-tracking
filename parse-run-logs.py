#!/usr/bin/env python3
import sys, os
import argparse
import re
import yaml

CATEGORIES=["linear-algebra/blas",
            "linear-algebra/kernels",
            "linear-algebra/solvers",
            "datamining",
            "stencils",
            "medley",
]

def kernels_list(target_dir):
    return [kernel for kernel in os.listdir(target_dir) if os.path.isdir(os.path.join(target_dir, kernel))]

def parse_log(target_dir, log_file):
    re_bench = re.compile(r'\[INFO\] Running (\d+) times (?P<path>.+)\.\.\.')
    re_time = re.compile(r'\[INFO\] Normalized time: (?P<time>.+)')
    timings = {}
    with open(log_file) as logf:
        bench = None
        for line in logf.readlines():
            if bench is None:
                m = re_bench.match(line)
                if m is not None:
                    bench = os.path.dirname(m.group("path"))
                    bench = bench.replace(f"{target_dir}/", "")
                    timings[bench] = []
            if bench is not None:
                m = re_time.match(line)
                if m is not None:
                    timing = float(m.group("time"))
                    timings[bench].append(timing)
    return timings

def parse_all(target_dir):
    benchmarks = {}
    for category in CATEGORIES:
        for kernel in kernels_list(os.path.join(target_dir, category)):
            benchmark = os.path.join(category, kernel)
            benchmarks[benchmark] = []
            timings = parse_log(target_dir, os.path.join(target_dir, category, kernel, "run-all.log"))
            benchmarks.update(timings)
    return benchmarks
        
def main(argv):
    parser = argparse.ArgumentParser(description="Polybench run log parser")
    parser.add_argument("-o", "--output", help="output yaml file")
    parser.add_argument("target_dir", help="polybench run dir")
    args = parser.parse_args(argv[1:])

    args.target_dir = args.target_dir.rstrip("/")
    benchmarks = parse_all(args.target_dir)
    outs = yaml.dump(benchmarks)
    if args.output is None:
        sys.stdout.write(outs)
    else:
        with open(args.output, "w") as outf:
            outf.write(outs)

if __name__ == "__main__":
    main(sys.argv)
