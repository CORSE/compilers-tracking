#!/usr/bin/env bash
set -euo pipefail
dir="$(dirname "$0")"
categories=('linear-algebra/blas'
            'linear-algebra/kernels'
            'linear-algebra/solvers'
            'datamining'
            'stencils'
            'medley')

outdir="${1-output}"
polybench="$outdir"/polybench

for cat in "${categories[@]}"; do
    for kerndir in "$polybench/$cat"/*; do
        kern="$(basename "$kerndir")"
        bash $polybench/utilities/time_benchmark.sh "$kerndir"/"$kern" 2>&1 | tee "$kerndir"/run-all.log
    done
done
