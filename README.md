Compilers versions comparison over time
=======================================

For a quick view of results ref to the `archives` dir.

Here are some performance evolution figures over gcc and clang versions:

- GCC -O3 SSE3 perf evolution: [GCC -O3 perf figure](archives/gcc-O3-sse3/compare-gcc-O3-sse3.svg)
- GCC -O2 SSE3 perf evolution: [GCC -O2 perf figure](archives/gcc-O2-sse3/compare-gcc-O2-sse3.svg)
- CLANG -O3 SSE3 perf evolution: [clang -O3 perf figure](archives/clang-O3-sse3/compare-clang-O3-sse3.svg)
- CLANG -O2 SSE3 perf evolution: [clang -O2 perf figure](archives/clang-O2-sse3/compare-clang-O2-sse3.svg)
- Merged GCC and CLANG -O3 perf evolution: [gcc and clang -O3 perf figure](archives/compare-gcc-clang-O3-sse3.svg)
- Merged GCC and CLANG -O2 perf evolution: [gcc and clang -O2 perf figure](archives/compare-gcc-clang-O2-sse3.svg)

And some compiler size evolution figures over gcc and clang versions:
- GCC Compiler Size evolution: [GCC compiler size figure](archives/compare-size-gcc.svg)
- CLANG Compiler Size evolution: [clang compiler size figure](archives/compare-size-clang.svg)


## Simple harness to compile polybench

The `polybench-*` subdir is an untouched extraction of the
polybench-c-4.2.1-beta.tar.gz archive.
Ref to https://sourceforge.net/projects/polybench/.

The `build-all.sh` and `run-all.sh` script offer the possibility
to differentiate the `CC_KERNEL` compiler for compiling the kernel
and the `CC` compiler for compiling the harness.

This is useful for compiling with different compilers the kernel
itself.

## Compilers installation

In order to install compilers from different versions
ref to [the installs directory](installs/README.md).

## Compiler size comparison

First install clang and gcc compilers versions as described above.

Generate sizes info for clang:

    for i in ~/work/compiler-explorer/install/clang-[23456789]* ~/work/compiler-explorer/install/clang-1*; do \
        du -hk $(readlink -e $i/bin/opt); done | tee archives/clang-sizes.txt

Generate sizes info for gcc:

    for i in ~/work/compiler-explorer/install/gcc-[23456789]* ~/work/compiler-explorer/install/gcc-1*; do \
        du -hk $(readlink -e $i/libexec/gcc/x86_64-linux-gnu/*/cc1); done | tee archives/gcc-sizes.txt

## Build Polybench for all compilers

Build for all gcc versions at O3 and O2 with:

    for opt in O3 O2; do for i in ~/work/compiler-explorer/install/gcc-*; do comp=$(basename $i); \
        env CC_KERNEL=$i/bin/gcc CFLAGS_KERNEL="-$opt -msse3" LDFLAGS=-static \
	./build-all.sh output-$comp-$opt-sse3-static || echo "  ***ERROR: $i"; done 2>&1 | tee build-gcc-$opt.log; done

Build for all clang versions at O3 and O2 with:

    for opt in O3 O2; do for i in ~/work/compiler-explorer/install/clang-*; do comp=$(basename $i); \
        env CC_KERNEL=$i/bin/clang CFLAGS_KERNEL="-$opt -msse3" LDFLAGS=-static \
	./build-all.sh output-$comp-$opt-sse3-static || echo "  ***ERROR: $i"; done 2>&1 | tee build-clang-$opt.log; done

## Run Polybench for all compilers

Run for gcc O3 and O2 with:

    for opt in O3 O2; do for i in ~/work/compiler-explorer/install/gcc-*; do comp=$(basename $i); \
    ./run-all.sh output-$comp-$opt-sse3-static || echo "  ***ERROR: $i"; done 2>&1 | tee run-gcc-$opt.log; done

Run for clang O3 and O2 with:

    for opt in O3 O2; do for i in ~/work/compiler-explorer/install/clang-*; do comp=$(basename $i); \
    ./run-all.sh output-$comp-$opt-sse3-static || echo "  ***ERROR: $i"; done 2>&1 | tee run-clang-$opt.log; done

## Parse run logs

Parse gcc run logs with:

      for i in output-gcc-*/polybench; do ./parse-run-logs.py $i -o $(dirname $i).yaml; done

Parse clang run logs with:

      for i in output-clang-*/polybench; do ./parse-run-logs.py $i -o $(dirname $i).yaml; done

## Generate performance graph

Generate gcc performance graph with:

      for opt in O3 O2; do ./compare-results.py --bench "Polybench -$opt" output-gcc-[23456789]*-$opt-sse3-static.yaml output-gcc-1*-$opt-sse3-static.yaml -o compare-gcc-$opt-sse3.svg; done

Note that the `archives/gcc-O*-sse3` directory contains the input `.yaml` files in order to reproduce the figure.

Generate clang performance graph with:

      for opt in O3 O2; do ./compare-results.py --bench "Polybench -$opt" output-clang-[23456789]*-$opt-sse3-static.yaml output-clang-1*-$opt-sse3-static.yaml -o compare-clang-$opt-sse3.svg; done

Note that the `archives/clang-O*-sse3` directory contains the input `.yaml` files in order to reproduce the figure.


## Generate compiler size graph

Generate gcc compiler size graph with:

      ./compare-results.py --size output-gcc-[23456789]*-O3-sse3-static.yaml output-gcc-1*-O3-sse3-static.yaml -o compare-size-gcc.svg

Generate clang compiler size graph with:

      ./compare-results.py --size output-clang-[23456789]*-O3-sse3-static.yaml output-clang-1*-O3-sse3-static.yaml -o compare-size-clang.svg


